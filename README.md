# DKX/Angular/FilesDropUpload

Form file uploader with drop zone without any styles.

Based on:

* [@dkx/ng-file-upload](https://gitlab.com/dkx/angular/file-upload)
* [@dkx/ng-files-drop-zone](https://gitlab.com/dkx/angular/files-drop-zone)

## Installation

```bash
$ npm install --save @dkx/ng-files-drop-upload
```

or with yarn

```bash
$ yarn add @dkx/ng-files-drop-upload
```

## Usage

**Module:**

```typescript
import {NgModule} from '@angular/core';
import {FilesDropUploadModule} from '@dkx/ng-files-drop-upload';

@NgModule({
	imports: [
		FilesDropUploadModule,
	],
})
export class AppModule {}
```

**Template:**

```html
<dkx-files-drop-upload #upload formControlName="files">
	Drop or
	<button (click)="upload.openUploadDialog()" type="button">select files</button>
	<hr>
	<ul>
		<li *ngFor="let file of upload.data">{{ file.name }}</li>
	</ul>
</dkx-files-drop-upload>
```

## Events

* `(changed)`: Dropped or selected files
