import {NgModule} from '@angular/core';
import {FilesDropZoneModule} from '@dkx/ng-files-drop-zone';
import {FileUploadModule} from '@dkx/ng-file-upload';

import {FilesDropUploadComponent} from './files-drop-upload.component';


@NgModule({
	imports: [
		FilesDropZoneModule,
		FileUploadModule,
	],
	declarations: [
		FilesDropUploadComponent,
	],
	exports: [
		FilesDropUploadComponent,
	],
})
export class FilesDropUploadModule {}
