import {AfterViewInit, Component, EventEmitter, Output, ViewChild} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {FileUploadComponent} from '@dkx/ng-file-upload';


@Component({
	selector: 'dkx-files-drop-upload',
	templateUrl: './files-drop-upload.component.html',
	styles: [
		`
			:host {
				display: block;
			}
			
			.dkx-files-drop-upload-zone {
				width: 100%;
				height: 100%;
			}
		`,
	],
	providers: [
		{
			provide: NG_VALUE_ACCESSOR,
			useExisting: FilesDropUploadComponent,
			multi: true,
		},
	],
})
export class FilesDropUploadComponent implements ControlValueAccessor, AfterViewInit
{


	@ViewChild(FileUploadComponent, {static: true})
	public inner: FileUploadComponent;

	@Output()
	public changed: EventEmitter<Array<Blob>> = new EventEmitter;


	public get data(): Array<Blob>
	{
		return this.inner.data;
	}


	public ngAfterViewInit(): void
	{
		if (this.inner) {
			this.inner.changed.subscribe((files) => this.changed.emit(files));
		}
	}


	public openUploadDialog(): void
	{
		if (this.inner) {
			this.inner.openDialog();
		}
	}


	public filesDropped(files: Array<File>): void
	{
		if (this.inner) {
			this.inner.setFiles(files);
		}
	}


	public registerOnChange(fn: any): void
	{
		if (this.inner) {
			this.inner.registerOnChange(fn);
		}
	}


	public registerOnTouched(fn: any): void
	{
		if (this.inner) {
			this.inner.registerOnTouched(fn);
		}
	}


	public setDisabledState(isDisabled: boolean): void
	{
		if (this.inner) {
			this.inner.setDisabledState(isDisabled);
		}
	}


	public writeValue(obj: any): void
	{
		if (this.inner) {
			this.inner.writeValue(obj);
		}
	}

}
